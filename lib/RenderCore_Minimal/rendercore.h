/* rendercore.h - Copyright 2019 Utrecht University

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <thread>
#pragma once

namespace lh2core
{

//  +-----------------------------------------------------------------------------+
//  |  Mesh                                                                       |
//  |  Minimalistic mesh storage.                                           LH2'19|
//  +-----------------------------------------------------------------------------+
class Mesh
{
public:
	float4* vertices = 0;							// vertex data received via SetGeometry
	int vcount = 0;									// vertex count
	CoreTri* triangles = 0;							// 'fat' triangle data
	
};

//Ripped from rasterizer
class Material
{
public:
	// constructor / destructor
	Material() = default;
	// data members
	float3 diffuse;		// diffuse material color
	int materialtype; // 0 is diffuse, 1 is mirror, 2 is glass
	// Texture* texture = 0;			
};

//  +-----------------------------------------------------------------------------+
//  |  RenderCore                                                                 |
//  |  Encapsulates device code.                                            LH2'19|
//  +-----------------------------------------------------------------------------+
class RenderCore
{
public:
	// methods
	void Init();
	void SetTarget( GLTexture* target );
	void SetGeometry( const int meshIdx, const float4* vertexData, const int vertexCount, const int triangleCount, const CoreTri* triangles, const uint* alphaFlags = 0 );
	void Render( const ViewPyramid& view, const Convergence converge, const float brightness, const float contrast );
	void Shutdown();
	std::tuple<float3, float3>  BoundingBox(Mesh mesh);
	std::tuple<float3, float3> BoundingBoxTopLevel();
	std::tuple<float3, float3>*  CreateAABBs();
	std::tuple<float3, float3>* boundingBoxes;
	float3 randomDirection();
	std::tuple < float, float3, uint> NearestIntersection(float3 rayOrigin, float3 rayVector, float tmax, std::tuple<float3, float3>*boxes);
	vector<Material*> matList;
	bool RayIntersectsAABB(float3 rayOrigin, float3 rayDirection, float tmax, std::tuple<float3, float3> box);
	float RayIntersectsTriangle(float3 rayOrigin, float3 rayVector, float tmax, float3 point0, float3 point1, float3 point2);
	float3 DirectIllumination(float3 I, float3 N, std::tuple<float3, float3> *boxes, int lightSource);
	float3 randomDirection(float3 N);
	float3 Trace(float3 rayOrigin, float3 rayVector, uint currentMaterialID, std::tuple<float3, float3> *boxes, bool leftside);
	uint createRBGfromfloats(float3 color);
	void renderParallel(const ViewPyramid& view, std::tuple<float3, float3>* boundingBoxes, float threadnum, float totalthreads);
	void RenderCore::SetMaterials(CoreMaterial* mat, const CoreMaterialEx* matEx, const int materialCount);
	int framenumber = 0, lightonecount, lighttwocount;
	const int Nlights = 2;
	float RR = 0.08;
	float RRINV = 1 / RR;
	float3* floatscreen = new float3[1280 * 720];
	bool preprocessingdone = false;
	std::tuple<float3, float3, float> lightSources[2];
	int xCells;
	int yCells;
	int zCells;
	int grid[50][50][50][2] = {};
	float xSpam, ySpam, zSpam, xStart, yStart, zStart;

	// internal methods
private:
	// data members	
	Bitmap* screen = 0;								// temporary storage of RenderCore output; will be copied to render target
	int targetTextureID = 0;						// ID of the target OpenGL texture
	vector<Mesh> meshes;							// mesh data storage

public:
	CoreStats coreStats;							// rendering statistics
};

} // namespace lh2core

// EOF